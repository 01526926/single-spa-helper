# Magicshop Micro Frontend with single-spa

This application acts as a helper repository for a Micro Frontend
case study implementation using [single-spa](https://single-spa.js.org/).

## Recommended use

It is recommend to clone this project and use it as a parent folder for 
the following git repositories, which one has to clone manually:

- https://gitlab.com/01526926/single-spa-root-config
- https://gitlab.com/01526926/single-spa-authentication
- https://gitlab.com/01526926/single-spa-navigation
- https://gitlab.com/01526926/single-spa-dashboard
- https://gitlab.com/01526926/single-spa-sales
- https://gitlab.com/01526926/single-spa-article

For deploying the applications, it is recommended to deploy and use the 
import map deployer, which is a copy of the [single-spa-imd](https://github.com/single-spa/import-map-deployer).
- https://gitlab.com/01526926/import-map-deployer

## Run the micro frontend applications locally

After cloning all required repos locally into the folder of this repo, you can serve all application with the following command.

```
npm start
```
